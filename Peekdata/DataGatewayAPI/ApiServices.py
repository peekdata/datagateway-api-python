"""
Class contains main methods to be used with Peekdata DataGateway API.

Public and available for testing server:
https://demo.peekdata.io:8443
"""

from Peekdata.DataGatewayAPI.Model import *
import requests
import os

__author__ = 'Dionizas Antipenkovas'
__email__ = 'd.antipenkovas@peekdata.io'


class ApiClient:

    CONST_API_METHOD_ENGINEVERSION = "/datagateway/rest/v1/engine/version"
    CONST_API_METHOD_HEALTHCHECK = "/datagateway/rest/v1/healthcheck"
    CONST_API_METHOD_GETSELECT = "/datagateway/rest/v1/query/get"
    CONST_API_METHOD_GETDATA = "/datagateway/rest/v1/data/get"
    CONST_API_METHOD_GETDATAOPTIMIZED = "/datagateway/rest/v1/data/getoptimized"
    CONST_API_METHOD_GETCSV = "/datagateway/rest/v1/file/get"
    CONST_API_METHOD_GETEXCEL = "/datagateway/rest/v1/file/getexcel"

    def __init__(self, url: str, port: int, scheme: str):
        """
        default constructor
        """
        self.BaseAddress = "{scheme}://{url}:{port}".format(
            scheme=scheme,
            url=url,
            port=port,
        )

    def getVersion(self):
        """
        method to get Engine Version
        """
        try:
            r = requests.get(
                "{}{}".format(
                    self.BaseAddress,
                    self.CONST_API_METHOD_ENGINEVERSION
                ),
                headers={'Content-type': 'application/json'}
            )
            r.raise_for_status()
            result = json.dumps(
                json.loads(r.text),
                sort_keys=True,
                indent=4
            )
            return result
        except requests.exceptions.RequestException as e:
            print("ERROR:\n  ", e)
        exit(1)

    def healthCheck(self):
        """
        method to check service availability
        """
        try:
            r = requests.get(
                "{}{}".format(
                    self.BaseAddress,
                    self.CONST_API_METHOD_HEALTHCHECK
                ),
                headers={'Content-type': 'application/json'},
            )
            r.raise_for_status()
            return r.ok
        except requests.exceptions.RequestException as e:
            print("ERROR:\n  ", e)
        exit(1)

    def getSelect(self, request):
        """
        method to get SELECT statement
        """
        json_request = serialize_to_json(request)
        try:
            r = requests.post(
                "{}{}".format(
                    self.BaseAddress,
                    self.CONST_API_METHOD_GETSELECT
                ),
                headers={'Content-type': 'application/json'},
                data=json_request,
            )
            r.raise_for_status()
            result = r.text.rstrip()
            return result
        except requests.exceptions.RequestException as e:
            print("ERROR:\n  ", e)
        exit(1)

    def getData(self, request):
        """
        method to get DATA
        """
        json_request = serialize_to_json(request)
        try:
            r = requests.post(
                "{}{}".format(
                    self.BaseAddress,
                    self.CONST_API_METHOD_GETDATA
                ),
                headers={'Content-type': 'application/json'},
                data=json_request,
            )
            r.raise_for_status()
            result = json.dumps(
                json.loads(r.text),
                sort_keys=True,
                indent=4
            )
            return result
        except requests.exceptions.RequestException as e:
            print("ERROR:\n  ", e)
        exit(1)

    def GetCSV(self, request, filename):
        """
        method to get CSV file
        """
        json_request = serialize_to_json(request)
        try:
            r = requests.post(
                "{}{}".format(
                    self.BaseAddress,
                    self.CONST_API_METHOD_GETCSV
                ),
                headers={'Content-type': 'application/json'},
                data=json_request,
            )
            r.raise_for_status()
            if os.path.isfile(filename):
                os.unlink(filename)
            with open(filename, 'w', encoding='utf-8') as f:
                f.write(r.text)
            return True
        except requests.exceptions.RequestException as e:
            print("ERROR:\n  ", e)
        exit(1)

    def GetExcel(self, request, filename):
        """
        method to get Excel file
        """
        json_request = serialize_to_json(request)
        try:
            r = requests.post(
                "{}{}".format(
                    self.BaseAddress,
                    self.CONST_API_METHOD_GETEXCEL
                ),
                headers={'Content-type': 'application/json'},
                data=json_request,
            )
            r.raise_for_status()
            if os.path.isfile(filename):
                os.unlink(filename)
            with open(filename, 'w', encoding='utf-16') as f:
                f.write(r.text)
            return True
        except requests.exceptions.RequestException as e:
            print("ERROR:\n  ", e)
        exit(1)